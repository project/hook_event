<?php

namespace Drupal\hook_event\Utility;

/**
 * Utility class to easily handle the hook names.
 */
class HookName {

  public const SEPARATOR = '_';

  /**
   * Constructs the hook name utility instance.
   *
   * @param string $baseHook
   *   The base hook name.
   */
  public function __construct(
    public string $baseHook
  ) {
  }

  /**
   * Creates new instance from given base hook.
   *
   * @param string $base_hook
   *   The base hook name.
   */
  public static function fromBaseHook(string $base_hook): static {
    return new static($base_hook);
  }

  /**
   * Getter for the base hook name.
   *
   * @return string
   *   The base hook name (rewritten for dynamic hooks).
   */
  public function getBaseHook(): string {
    return $this->rewriteHook($this->baseHook);
  }

  /**
   * Gets the full hook name (the one from the api).
   *
   * It basically adds the `hook` prefix.
   * Useful to search for the api definition.
   *
   * @return string
   *   The name of the hook.
   */
  public function toFullHookName(): string {
    return $this->merge(['hook', $this->getBaseHook()]);
  }

  /**
   * Gets the hook alter name.
   *
   *  The base hook + alter suffix, ex. form_alter.
   *
   * @return string
   *   The alter name.
   */
  public function toAlter(): string {
    return $this->merge([$this->getBaseHook(), 'alter']);
  }

  /**
   * Gets the full hook alter name (the one from the api).
   *
   *  The full hook + alter suffix, ex. hook_form_alter.
   *
   * @return string
   *   The alter hook name.
   */
  public function toFullAlterHookName(): string {
    return $this->merge([$this->toFullHookName(), 'alter']);
  }

  /**
   * The hook name as an event name.
   *
   * @return string
   *   The standardized hook event name.
   */
  public function toEventName(): string {
    return $this->toFullHookName();
  }

  /**
   * The hook alter name as an event name.
   *
   * @return string
   *   The standardized hook event name.
   */
  public function toAlterEventName(): string {
    return $this->toFullAlterHookName();
  }

  /**
   * Merges all the hook name parts into a string.
   *
   * @param array $partials
   *   The parts of the hook name.
   *
   * @return string
   *   The final name.
   */
  public function merge(array $partials): string {
    return implode(static::SEPARATOR, $partials);
  }

  /**
   * Rewrites the given hook if is considered as a pattern hook.
   *
   * @param string $base_hook
   *   The original hook.
   *
   * @return string
   *   The hook name.
   */
  public function rewriteHook(string $base_hook): string {
    $patterns = $this->getPatternHooks();
    foreach ($patterns as $pattern => $hook) {
      if (str_starts_with($base_hook, $pattern) && !str_ends_with($base_hook, '_alter')) {
        return $hook;
      }
    }

    return $base_hook;
  }

  /**
   * Gets the pattern (dynamic) hooks.
   *
   * The list of all hooks to be considered as dynamics,
   * for example the theme suggestion has a _HOOK,
   * which depends on the theme hooks.
   *
   * @return string[]
   *   The list of pattern hooks.
   *
   * @todo This maybe kind of a workaround, it's possible that we will have to
   *   do the proper processing in the hook definitions processor.
   */
  protected function getPatternHooks(): array {
    return [
      'theme_suggestions_' => 'theme_suggestions_hook',
    ];
  }

}
