<?php

namespace Drupal\hook_event\Extension;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\hook_event\Event\HookEventInterface;
use Drupal\hook_event\Event\HookInvokeEventInterface;
use Drupal\hook_event\Negotiator\HookEventNegotiatorInterface;
use Drupal\hook_event\Storage\HookDefinitionStorageInterface;
use Drupal\hook_event\Utility\HookName;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use function array_filter;
use function array_search;
use function array_values;
use function is_array;
use function reset;

/**
 * Decorates the core module handler service to provide hook event dispatching.
 */
class ModuleHandler implements ModuleHandlerInterface {
  use ModuleHandlerImplementMethodsTrait;

  /**
   * The hook definition storage.
   *
   * @var \Drupal\hook_event\Storage\HookDefinitionStorageInterface
   */
  protected HookDefinitionStorageInterface $hookDefinitionStorage;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The hook event negotiator.
   *
   * @var \Drupal\hook_event\Negotiator\HookEventNegotiatorInterface
   */
  protected HookEventNegotiatorInterface $hookEventNegotiator;

  /**
   * Constructs module handler decorator.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The original (inner) module handler service.
   * @param \Drupal\hook_event\Storage\HookDefinitionStorageInterface $hook_definition_storage
   *   The hook definition storage.
   * @param \Drupal\hook_event\Negotiator\HookEventNegotiatorInterface $hook_event_negotiator
   *   The hook event negotiator.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher.
   */
  public function __construct(
    ModuleHandlerInterface $module_handler,
    HookDefinitionStorageInterface $hook_definition_storage,
    HookEventNegotiatorInterface $hook_event_negotiator,
    EventDispatcherInterface $event_dispatcher
  ) {
    $this->baseHandler = $module_handler;
    $this->hookDefinitionStorage = $hook_definition_storage;
    $this->hookEventNegotiator = $hook_event_negotiator;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public function invokeAllWith(string $hook, callable $callback): void {
    $this->baseHandler->invokeAllWith($hook, $callback);

    // The hook_event module is responsible for invoking, so let's use its name.
    $module = 'hook_event';
    $reflection = new \ReflectionFunction($callback);
    $args = $reflection->getStaticVariables();
    $event = $this->invokeAsEvent('hook_event', $hook, $args);

    if ($event->hasReturnValue()) {
      $callback(function () use ($event) {
        return $event->getReturnValue();
      }, $module);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function invoke($module, $hook, array $args = []) {
    // The module has its own implementation.
    // We don't want to mix the events with the hooks in that case.
    if ($this->hasImplementations($hook, $module)) {
      return $this->baseHandler->invoke($module, $hook, $args);
    }
    $event = $this->invokeAsEvent($module, $hook, $args);

    return $event->hasReturnValue() ? $event->getReturnValue() : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function invokeAll($hook, array $args = []) {
    $return = $this->baseHandler->invokeAll($hook, $args);

    $hook_name = HookName::fromBaseHook($hook);
    $arguments = $this->mapHookArguments($hook_name->toFullHookName(), $args);
    $eventObject = $this->hookEventNegotiator->getInvokeEventForHook($hook_name->toFullHookName());
    $event = new $eventObject($hook, $arguments);

    $this->eventDispatcher->dispatch($event, $hook_name->toEventName());
    $this->updateArguments($args, $event);

    if (!$event->hasReturnValue()) {
      return $return;
    }

    $event_return = $event->getReturnValue();
    if (is_array($event_return)) {
      $return = NestedArray::mergeDeep($return, $event_return);
    }
    else {
      $return[] = $event_return;
    }

    return $return;
  }

  /**
   * {@inheritdoc}
   */
  public function alter($type, &$data, &$context1 = NULL, &$context2 = NULL) {
    $this->baseHandler->alter($type, $data, $context1, $context2);

    $base_hook_name = is_array($type) ? reset($type) : $type;
    $hook_name = HookName::fromBaseHook($base_hook_name);

    $arguments = [
      'data' => $data,
      'context1' => $context1,
      'context2' => $context2,
    ];

    $event_arguments = $this->mapHookArguments($hook_name->toFullAlterHookName(), $arguments);
    $eventObject = $this->hookEventNegotiator->getAlterEventForHook($hook_name->toFullHookName());
    $event = new $eventObject($hook_name->toAlter(), $event_arguments);

    if (is_array($type)) {
      foreach ($type as $type_hook) {
        $event_type_hook = HookName::fromBaseHook($type_hook);
        $event->setHook($event_type_hook->toAlter());
        $this->eventDispatcher->dispatch($event, $event_type_hook->toAlterEventName());
      }
    }
    else {
      $this->eventDispatcher->dispatch($event, $hook_name->toAlterEventName());
    }

    foreach ($arguments as $name => $value) {
      $key = array_search($value, $event_arguments, TRUE);
      $$name = $event->getArgument($key, $$name);
    }
  }

  /**
   * Handles event dispatching on hooks invoking.
   *
   * @param string $module
   *   The hook module name.
   * @param string $hook
   *   The hook.
   * @param array $args
   *   The hook arguments.
   *
   * @return \Drupal\hook_event\Event\HookInvokeEventInterface
   *   The event.
   */
  protected function invokeAsEvent(string $module, string $hook, array $args): HookInvokeEventInterface {
    $hook_name = HookName::fromBaseHook($hook);
    $arguments = $this->mapHookArguments($hook_name->toFullHookName(), $args);
    $eventObject = $this->hookEventNegotiator->getInvokeEventForHook($hook_name->toFullHookName());
    $event = new $eventObject($hook, $arguments);
    $event->setModuleName($module);

    $this->eventDispatcher->dispatch($event, $hook_name->toEventName());
    $this->updateArguments($args, $event);

    return $event;
  }

  /**
   * Maps arguments keys based on the hook arguments names.
   *
   * @param string $hook
   *   The hook function name.
   * @param array $arguments
   *   The hook arguments.
   *
   * @return array
   *   The mapped arguments (if hook was registered).
   */
  protected function mapHookArguments(string $hook, array $arguments = []): array {
    if (!$this->hookDefinitionStorage->hasHookDefinition($hook)) {
      return array_values($arguments);
    }

    $map = [];
    $hook_arguments = $this->hookDefinitionStorage->getHookArguments($hook);
    $position = 0;
    foreach (array_filter($arguments) as $key => $value) {
      $map[$hook_arguments[$position] ?? $key] = $value;
      $position++;
    }

    return $map;
  }

  /**
   * Updates the argument values.
   *
   * Useful in case of the arguments passed by the reference.
   *
   * @param array $arguments
   *   The list of original arguments.
   * @param \Drupal\hook_event\Event\HookEventInterface $event
   *   The event object containing updated values.
   */
  protected function updateArguments(array &$arguments, HookEventInterface $event): void {
    foreach ($event->getArguments(as_numeric: TRUE) as $key => $value) {
      $arguments[$key] = $value;
    }
  }

}
