<?php

declare(strict_types=1);

namespace Drupal\hook_event\Extension;

use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides default implementation for methods required by the interface.
 */
trait ModuleHandlerImplementMethodsTrait {

  /**
   * The base module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $baseHandler;

  /**
   * {@inheritdoc}
   */
  public function load($name) {
    return $this->baseHandler->load($name);
  }

  /**
   * {@inheritdoc}
   */
  public function loadAll() {
    $this->baseHandler->loadAll();
  }

  /**
   * {@inheritdoc}
   */
  public function isLoaded() {
    return $this->baseHandler->isLoaded();
  }

  /**
   * {@inheritdoc}
   */
  public function reload() {
    $this->baseHandler->reload();
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleList() {
    return $this->baseHandler->getModuleList();
  }

  /**
   * {@inheritdoc}
   */
  public function getModule($name) {
    return $this->baseHandler->getModule($name);
  }

  /**
   * {@inheritdoc}
   */
  public function setModuleList(array $module_list = []) {
    $this->baseHandler->setModuleList($module_list);
  }

  /**
   * {@inheritdoc}
   */
  public function addModule($name, $path) {
    $this->baseHandler->addModule($name, $path);
  }

  /**
   * {@inheritdoc}
   */
  public function addProfile($name, $path) {
    $this->baseHandler->addProfile($name, $path);
  }

  /**
   * {@inheritdoc}
   */
  public function buildModuleDependencies(array $modules) {
    return $this->baseHandler->buildModuleDependencies($modules);
  }

  /**
   * {@inheritdoc}
   */
  public function moduleExists($module) {
    return $this->baseHandler->moduleExists($module);
  }

  /**
   * {@inheritdoc}
   */
  public function loadAllIncludes($type, $name = NULL) {
    return $this->baseHandler->loadAllIncludes($type, $name);
  }

  /**
   * {@inheritdoc}
   */
  public function loadInclude($module, $type, $name = NULL) {
    return $this->baseHandler->loadInclude($module, $type, $name);
  }

  /**
   * {@inheritdoc}
   */
  public function getHookInfo() {
    return $this->baseHandler->getHookInfo();
  }

  /**
   * {@inheritdoc}
   */
  public function getImplementations($hook) {
    return $this->baseHandler->getImplementations($hook);
  }

  /**
   * {@inheritdoc}
   */
  public function writeCache() {
    $this->baseHandler->writeCache();
  }

  /**
   * {@inheritdoc}
   */
  public function resetImplementations() {
    $this->baseHandler->resetImplementations();
  }

  /**
   * {@inheritdoc}
   */
  public function hasImplementations(string $hook, $modules = NULL): bool {
    return $this->baseHandler->hasImplementations($hook, $modules);
  }

  /**
   * {@inheritdoc}
   */
  public function implementsHook($module, $hook) {
    return $this->baseHandler->implementsHook($module, $hook);
  }

  /**
   * {@inheritdoc}
   */
  public function invokeDeprecated($description, $module, $hook, array $args = []) {
    return $this->baseHandler->invokeDeprecated($description, $module, $hook, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function invokeAllDeprecated($description, $hook, array $args = []) {
    return $this->baseHandler->invokeAllDeprecated($description, $hook, $args);
  }

  /**
   * {@inheritdoc}
   */
  public function alterDeprecated($description, $type, &$data, &$context1 = NULL, &$context2 = NULL) {
    $this->baseHandler->alterDeprecated($description, $type, $data, $context1, $context2);
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleDirectories() {
    return $this->baseHandler->getModuleDirectories();
  }

  /**
   * {@inheritdoc}
   */
  public function getName($module) {
    return $this->baseHandler->getName($module);
  }

}
