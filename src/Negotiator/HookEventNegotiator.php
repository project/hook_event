<?php

declare(strict_types=1);

namespace Drupal\hook_event\Negotiator;

use Drupal\hook_event\Discovery\HookEventsDiscoveryInterface;
use Drupal\hook_event\Event\HookAlterEvent;
use Drupal\hook_event\Event\HookInvokeEvent;
use Drupal\hook_event\Event\HookPreprocessEvent;

/**
 * Negotiates the event classes to be used when dispatching hook as an event.
 */
class HookEventNegotiator implements HookEventNegotiatorInterface {

  /**
   * Creates the negotiator instance.
   *
   * @param \Drupal\hook_event\Discovery\HookEventsDiscoveryInterface $hookEventsDiscovery
   *   The hook events discovery.
   */
  public function __construct(protected HookEventsDiscoveryInterface $hookEventsDiscovery) {
  }

  /**
   * {@inheritdoc}
   */
  public function getInvokeEventForHook(string $hook): string {
    return $this->getEventFromRegistry($hook) ?? HookInvokeEvent::class;
  }

  /**
   * {@inheritdoc}
   */
  public function getAlterEventForHook(string $hook): string {
    return $this->getEventFromRegistry($hook) ?? HookAlterEvent::class;
  }

  /**
   * {@inheritdoc}
   */
  public function getPreprocessEventForHook(string $hook): string {
    return $this->getEventFromRegistry($hook) ?? HookPreprocessEvent::class;
  }

  /**
   * Gets the event class from registry (if available).
   *
   * @param string $hook
   *   The hook for which the class should be registered.
   *
   * @return string|null
   *   The registered class or null.
   */
  private function getEventFromRegistry(string $hook): ?string {
    if (FALSE === $this->hookEventsDiscovery->hasDefinition($hook)) {
      return NULL;
    }
    $class = $this->hookEventsDiscovery->getDefinition($hook);

    return is_string($class) && class_exists($class) ? $class : NULL;
  }

}
