<?php

declare(strict_types=1);

namespace Drupal\hook_event\Negotiator;

/**
 * Defines the interface for the hook events class negotiators.
 */
interface HookEventNegotiatorInterface {

  /**
   * Gets the invoke event by hook name.
   *
   * @param string $hook
   *   The hook name.
   *
   * @return string
   *   The invoke event class.
   */
  public function getInvokeEventForHook(string $hook): string;

  /**
   * Gets the alter event by hook name.
   *
   * @param string $hook
   *   The hook name.
   *
   * @return string
   *   The alter event class.
   */
  public function getAlterEventForHook(string $hook): string;

  /**
   * Gets the preprocess event by hook name.
   *
   * @param string $hook
   *   The hook name.
   *
   * @return string
   *   The preprocess event class.
   */
  public function getPreprocessEventForHook(string $hook): string;

}
