<?php

declare(strict_types=1);

namespace Drupal\hook_event\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Provides the event class for the API discovery events.
 */
class ApiDiscoveryEvent extends Event implements ApiDiscoveryEventInterface {

  /**
   * Creates the api discovery event object.
   *
   * @param array $discoveredDefinitions
   *   The array of definitions registered by the discovery.
   */
  public function __construct(
    protected array $discoveredDefinitions
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function getDiscoveredDefinitions(): array {
    return $this->discoveredDefinitions;
  }

  /**
   * {@inheritdoc}
   */
  public function setDiscoveredDefinitions(array $discoveredDefinitions): void {
    $this->discoveredDefinitions = $discoveredDefinitions;
  }

  /**
   * {@inheritdoc}
   */
  public function addDefinitions(array $definition): void {
    $this->discoveredDefinitions[] = $definition;
  }

}
