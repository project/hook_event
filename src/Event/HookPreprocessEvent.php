<?php

declare(strict_types=1);

namespace Drupal\hook_event\Event;

use Drupal\Component\Utility\NestedArray;

/**
 * Provides the event class used for preprocess hooks.
 */
class HookPreprocessEvent extends HookEvent implements HookPreprocessEventInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(string $hook, array $arguments = []) {
    parent::__construct($hook, [
      'variables' => $arguments,
      'raw_variables' => $arguments,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function setVariables(array $variables): void {
    $this->setArgument('variables', $variables);
  }

  /**
   * {@inheritdoc}
   */
  public function getVariables(): array {
    return $this->getArgument('variables') ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getRawVariables(): array {
    return $this->getArgument('raw_variables');
  }

  /**
   * {@inheritdoc}
   */
  public function merge(array $variables): void {
    $variables = NestedArray::mergeDeep($this->getVariables(), $variables);
    $this->setVariables($variables);
  }

}
