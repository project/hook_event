<?php

declare(strict_types=1);

namespace Drupal\hook_event\Event;

use function is_null;

/**
 * Provides the event class used for invoked hooks.
 */
class HookInvokeEvent extends HookEvent implements HookInvokeEventInterface {

  /**
   * The return value to be passed as hook return value.
   *
   * @var mixed|null
   *   The value that should be returned (if any).
   */
  protected mixed $returnValue = NULL;

  /**
   * {@inheritdoc}
   */
  public function getReturnValue(): mixed {
    return $this->returnValue;
  }

  /**
   * {@inheritdoc}
   */
  public function setReturnValue(mixed $value): static {
    $this->returnValue = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasReturnValue(): bool {
    return !is_null($this->returnValue);
  }

}
