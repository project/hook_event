<?php

namespace Drupal\hook_event\Event;

/**
 * Provides the interface for the generic hook event.
 */
interface HookEventInterface {

  /**
   * Getter for the defined arguments.
   *
   * @param bool $as_numeric
   *   The flag to determine if the arguments array
   *   should be indexed by numbers.
   *
   * @return array
   *   The hook arguments.
   */
  public function getArguments(bool $as_numeric = FALSE): array;

  /**
   * Setter for the defined arguments.
   *
   * @param array $arguments
   *   The arguments to be set.
   */
  public function setArguments(array $arguments): static;

  /**
   * Gets the argument from the arguments by the name.
   *
   * @param string $name
   *   The name of the argument (from the hook variable name).
   * @param mixed $default_value
   *   The default value if argument is not present.
   *
   * @return mixed
   *   The argument value or default value.
   */
  public function getArgument(string $name, mixed $default_value = NULL): mixed;

  /**
   * Sets the given argument value.
   *
   * @param string $name
   *   The name of the argument (from the hook variable name).
   * @param mixed $value
   *   The argument value.
   */
  public function setArgument(string $name, mixed $value): static;

  /**
   * Gets the event hook name.
   *
   * @return string
   *   The hook name.
   */
  public function getHook(): string;

  /**
   * Sets the event hook name.
   */
  public function setHook(string $hook): static;

  /**
   * Gets the module name if present.
   *
   * @return string|null
   *   The module name defaults to null.
   */
  public function getModuleName(): ?string;

  /**
   * Sets the module name.
   *
   * @param string $name
   *   The module name.
   */
  public function setModuleName(string $name): static;

  /**
   * Checks if the module name was defined.
   *
   * @return bool
   *   True if the name was provided, false otherwise.
   */
  public function hasModule(): bool;

}
