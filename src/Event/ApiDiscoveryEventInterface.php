<?php

namespace Drupal\hook_event\Event;

/**
 * Provides the interface for the API discovery events.
 */
interface ApiDiscoveryEventInterface {

  public const PRE_SAVE = 'hook_event.api_discovery.pre_save';

  /**
   * Getter for the definitions.
   *
   * @return array
   *   The definitions.
   */
  public function getDiscoveredDefinitions(): array;

  /**
   * Setter for the definitions.
   *
   * @param array $discoveredDefinitions
   *   The definitions to be set.
   */
  public function setDiscoveredDefinitions(array $discoveredDefinitions): void;

  /**
   * Adds a single definition to the list of the definitions.
   *
   * @param array $definition
   *   The definition to be added.
   */
  public function addDefinitions(array $definition): void;

}
