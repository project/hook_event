<?php

namespace Drupal\hook_event\Event;

/**
 * Provides the interface for the alter hook event.
 */
interface HookAlterEventInterface extends HookEventInterface {}
