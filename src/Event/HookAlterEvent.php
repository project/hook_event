<?php

declare(strict_types=1);

namespace Drupal\hook_event\Event;

/**
 * Provides the event class used for the alter hooks.
 */
class HookAlterEvent extends HookEvent implements HookAlterEventInterface {}
