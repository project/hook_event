<?php

namespace Drupal\hook_event\Event;

/**
 * Defines the interface for the invoked hooks events.
 */
interface HookInvokeEventInterface extends HookEventInterface {

  /**
   * Getter for the return value.
   *
   * @return mixed
   *   The value to be returned.
   */
  public function getReturnValue(): mixed;

  /**
   * Setter for the return value.
   *
   * @param mixed $value
   *   The return vlaue to be set.
   */
  public function setReturnValue(mixed $value): static;

  /**
   * Check if the return value was set.
   *
   * @return bool
   *   True if return value was defined false otherwise.
   */
  public function hasReturnValue(): bool;

}
