<?php

declare(strict_types=1);

namespace Drupal\hook_event\Event;

use Symfony\Contracts\EventDispatcher\Event;
use function array_values;
use function is_string;

/**
 * Provides the base event class for the hook data to be dispatched as an event.
 */
abstract class HookEvent extends Event implements HookEventInterface {

  /**
   * The list of arguments passed to the hook.
   *
   * @var array
   *   The hook arguments, keyed by argument names
   *   if it was provided by the API discovery service.
   */
  protected array $arguments = [];

  /**
   * The name of the invoked hook.
   *
   * @var string
   *   The hook name.
   */
  private string $hook;

  /**
   * The module name.
   *
   * @var string|null
   *   The called module name (if any).
   */
  private ?string $moduleName = NULL;

  /**
   * Constructs the hook event object.
   *
   * @param string $hook
   *   The event hook name.
   * @param array $arguments
   *   The arguments to be processed by the event.
   */
  public function __construct(string $hook, array $arguments = []) {
    $this->hook = $hook;
    $this->setArguments($arguments);
  }

  /**
   * {@inheritdoc}
   */
  public function getArguments(bool $as_numeric = FALSE): array {
    return $as_numeric ? array_values($this->arguments) : $this->arguments;
  }

  /**
   * {@inheritdoc}
   */
  public function setArguments(array $arguments): static {
    $this->arguments = $arguments;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getArgument(string $name, mixed $default_value = NULL): mixed {
    return $this->getArguments()[$name] ?? $default_value;
  }

  /**
   * {@inheritdoc}
   */
  public function setArgument(string $name, mixed $value): static {
    $this->arguments[$name] = $value;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getHook(): string {
    return $this->hook;
  }

  /**
   * {@inheritdoc}
   */
  public function setHook(string $hook): static {
    $this->hook = $hook;

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleName(): ?string {
    return $this->moduleName;
  }

  /**
   * {@inheritdoc}
   */
  public function setModuleName(string $name): static {
    if (!$this->hasModule()) {
      $this->moduleName = $name;
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function hasModule(): bool {
    return is_string($this->getModuleName());
  }

}
