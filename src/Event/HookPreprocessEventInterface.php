<?php

declare(strict_types=1);

namespace Drupal\hook_event\Event;

/**
 * Provides the interface used for preprocess events.
 */
interface HookPreprocessEventInterface {

  /**
   * Setter for the variables.
   *
   * @param array $variables
   *   The render array variables.
   */
  public function setVariables(array $variables): void;

  /**
   * Getter for the variables.
   *
   * @return array
   *   The render array variables.
   */
  public function getVariables(): array;

  /**
   * Getter for the raw (untouched) variables.
   *
   * Useful in case of verifying the changes on the original
   * array with the variables.
   *
   * @return array
   *   The render array variables.
   */
  public function getRawVariables(): array;

  /**
   * Merge new variables to the existing ones.
   *
   * @param array $variables
   *   The render array variables.
   */
  public function merge(array $variables): void;

}
