<?php

declare(strict_types=1);

namespace Drupal\hook_event\Processor;

use function array_key_exists;

/**
 * Provides the definitions processors service collector.
 */
class HookDefinitionsProcessor implements HookDefinitionsProcessorInterface {

  /**
   * Array of registered processors.
   *
   * @var array
   */
  protected array $processors = [];

  /**
   * Register the discovered processors.
   *
   * @param \Drupal\hook_event\Resolver\HookDefinitionsProcessorInterface $processor
   *   The processor.
   *
   * @throws \Exception
   */
  public function register(HookDefinitionsProcessorInterface $processor): void {
    $type = $processor::class;

    if ($this->isRegistered($type)) {
      throw new \Exception(sprintf('Hook processor for %s type does already exist.', $type));
    }

    $this->processors[$type] = $processor;
  }

  /**
   * Pass the hook definitions through all the processors.
   *
   * @param array $definitions
   *   The hook definitions.
   */
  public function process(array &$definitions): void {
    foreach ($this->processors as $processor) {
      $processor->process($definitions);
    }
  }

  /**
   * Checks if the given processor was already registered.
   */
  public function isRegistered(string $type): bool {
    return array_key_exists($type, $this->processors);
  }

}
