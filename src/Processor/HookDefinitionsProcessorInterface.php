<?php

namespace Drupal\hook_event\Processor;

/**
 * Provides the interface for the hook definitions processors.
 */
interface HookDefinitionsProcessorInterface {

  /**
   * Process the hook definitions in order to apply the case-specific changes.
   *
   * @param array $definitions
   *   The registered hook definitions.
   */
  public function process(array &$definitions): void;

}
