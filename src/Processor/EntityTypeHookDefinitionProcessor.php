<?php

declare(strict_types=1);

namespace Drupal\hook_event\Processor;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Resolves the hook definitions related to the entity types.
 */
class EntityTypeHookDefinitionProcessor implements HookDefinitionsProcessorInterface {

  public const HOOK_PLACEHOLDER = '_entity_type_';

  /**
   * Creates the entity type hooks resolver service.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public function process(array &$definitions): void {
    $entity_types = $this->getEntityTypesIds();
    $entity_type_hooks = $this->getEntityTypeHooksFromDefinitions($definitions);
    foreach ($entity_types as $entity_id) {
      foreach ($entity_type_hooks as $hook => $definition) {
        $entity_hook_name = str_replace(static::HOOK_PLACEHOLDER, "_${entity_id}_", $hook);
        $definitions[$entity_hook_name] = $definition;
      }
    }
  }

  /**
   * Gets the hook definitions related to the entity types.
   *
   * @param array $definitions
   *   The registered hook definitions.
   *
   * @return array
   *   The entity type hooks.
   */
  protected function getEntityTypeHooksFromDefinitions(array $definitions): array {
    return array_filter(
      $definitions,
      fn($hook) => str_contains($hook, static::HOOK_PLACEHOLDER),
      ARRAY_FILTER_USE_KEY);
  }

  /**
   * Gets all the registered entity types.
   *
   * @return array
   *   The entity types ID's.
   */
  protected function getEntityTypesIds(): array {
    return array_keys($this->entityTypeManager->getDefinitions());
  }

}
