<?php

declare(strict_types=1);

namespace Drupal\hook_event\Discovery;

/**
 * Provides the interface to get the hooks definitions data.
 */
interface ApiDiscoveryInterface {

  public const CID = 'hook_event:hook_registry';

}
