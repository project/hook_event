<?php

declare(strict_types=1);

namespace Drupal\hook_event\Discovery;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\File\FileSystemInterface;
use function sprintf;

/**
 * Provides the base discovery class.
 */
abstract class DiscoveryBase implements DiscoveryInterface {

  /**
   * The registered definitions.
   *
   * @var array
   */
  protected array $definitions = [];

  /**
   * Creates the discovery instance.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The cache backend.
   */
  public function __construct(
    protected FileSystemInterface $fileSystem,
    protected CacheBackendInterface $cacheBackend,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  abstract public function registerDefinitions(): void;

  /**
   * {@inheritdoc}
   */
  public function getDefinitions(): array {
    $cache = $this->cacheBackend->get(static::CID);
    if ($cache && $cache->data) {
      return (array) $cache->data;
    }

    return $this->definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefinition(string $name): ?string {
    return $this->getDefinitions()[$name] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function hasDefinition(string $name): bool {
    return NULL !== $this->getDefinition($name);
  }

  /**
   * {@inheritdoc}
   */
  abstract public function getExtension(): string;

  /**
   * Gets all the founded files.
   *
   * @return array
   *   The list of the extension files.
   */
  protected function getExtensionFiles(): array {
    return $this->fileSystem->scanDirectory(DRUPAL_ROOT, $this->getExtensionMask());
  }

  /**
   * Gets the extension mask to be used in scanning process.
   *
   * @return string
   *   The mask used by the extension.
   */
  protected function getExtensionMask(): string {
    return sprintf('/\%s$/', $this->getExtension());
  }

}
