<?php

declare(strict_types=1);

namespace Drupal\hook_event\Discovery;

use Drupal\Component\Discovery\YamlDiscovery;

/**
 * Provides the hook events discovery.
 */
class HookEventsDiscovery extends DiscoveryBase implements HookEventsDiscoveryInterface {

  /**
   * The YAML discovery.
   *
   * @var \Drupal\Component\Discovery\YamlDiscovery|null
   */
  protected ?YamlDiscovery $yamlDiscovery = NULL;

  /**
   * {@inheritdoc}
   */
  public function getExtension(): string {
    return '.hook_events.yml';
  }

  /**
   * {@inheritdoc}
   */
  public function registerDefinitions(): void {
    $definitions = [];
    foreach ($this->getYamlDiscovery()->findAll() as $hooks) {
      $definitions = $hooks + $definitions;
    }

    $this->cacheBackend->set(static::CID, $definitions);
    $this->definitions = $definitions;
  }

  /**
   * Gets the YAML discovery.
   *
   * @return \Drupal\Component\Discovery\YamlDiscovery
   *   The YAML discovery.
   */
  protected function getYamlDiscovery(): YamlDiscovery {
    if (NULL === $this->yamlDiscovery) {
      $this->yamlDiscovery = new YamlDiscovery('hook_events', $this->getExtensionsDirectories());
    }

    return $this->yamlDiscovery;
  }

  /**
   * Gets the extensions directories.
   *
   * @return array
   *   The directories.
   */
  protected function getExtensionsDirectories(): array {
    // @todo These services are loaded statically because the circular reference
    //   we have to find a better option to make it work.
    $moduleHandler = \Drupal::moduleHandler();
    /** @var \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler */
    $themeHandler = \Drupal::service('theme_handler');

    return $moduleHandler->getModuleDirectories() + $themeHandler->getThemeDirectories();
  }

}
