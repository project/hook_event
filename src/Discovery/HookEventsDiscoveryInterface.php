<?php

declare(strict_types=1);

namespace Drupal\hook_event\Discovery;

/**
 * Provides the interface to get the hook events definitions data.
 */
interface HookEventsDiscoveryInterface {

  public const CID = 'hook_event:hook_events';

}
