<?php

declare(strict_types=1);

namespace Drupal\hook_event\Discovery;

/**
 * Defines the common discovery services interface.
 */
interface DiscoveryInterface {

  /**
   * Builds and register the discovered hook definitions.
   */
  public function registerDefinitions(): void;

  /**
   * Gets the registered definitions.
   *
   * Based on the implementation this method can load
   * the definitions from cache data first, if not found, then it is
   * trying to rebuild the registry.
   *
   * @return array
   *   The registered definitions.
   */
  public function getDefinitions(): array;

  /**
   * Gets the single definition.
   *
   * @param string $name
   *   The name/key of the definition.
   *
   * @return string|null
   *   The stored value.
   */
  public function getDefinition(string $name): ?string;

  /**
   * Checks if the given definition is defined.
   *
   * @param string $name
   *   The name/key of the definition.
   *
   * @return bool
   *   True if is defined, false otherwise.
   */
  public function hasDefinition(string $name): bool;

  /**
   * Gets the name of file extension to be used for the discovery.
   *
   * @return string
   *   The file extension.
   */
  public function getExtension(): string;

}
