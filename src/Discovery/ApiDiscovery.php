<?php

declare(strict_types=1);

namespace Drupal\hook_event\Discovery;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\hook_event\Event\ApiDiscoveryEvent;
use Drupal\hook_event\Event\ApiDiscoveryEventInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use function array_values;
use function array_diff;
use function file_exists;
use function get_defined_functions;

/**
 * Provides the hook API discovery.
 */
class ApiDiscovery extends DiscoveryBase implements ApiDiscoveryInterface {

  /**
   * Creates the api discovery instance.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The cache backend.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   */
  public function __construct(
    protected FileSystemInterface $fileSystem,
    protected CacheBackendInterface $cacheBackend,
    protected EventDispatcherInterface $eventDispatcher
  ) {
    parent::__construct($this->fileSystem, $this->cacheBackend);
  }

  /**
   * {@inheritdoc}
   */
  public function getExtension(): string {
    return '.api.php';
  }

  /**
   * {@inheritdoc}
   */
  public function registerDefinitions(): void {
    $hooks = array_flip($this->getDiscoveredHookFunctions());

    foreach ($hooks as $hook => $value) {
      $hooks[$hook] = [
        'arguments' => $this->buildHookArgumentsInfo($hook),
      ];
    }

    $event = new ApiDiscoveryEvent($hooks);
    $this->eventDispatcher->dispatch($event, ApiDiscoveryEventInterface::PRE_SAVE);
    $hooks = $event->getDiscoveredDefinitions();

    $this->cacheBackend->set(static::CID, $hooks);
    $this->definitions = $hooks;
  }

  /**
   * Builds the hook arguments data using reflection.
   *
   * @param string $hook_function
   *   The hook function name.
   *
   * @return array
   *   The mapped hook arguments.
   *
   * @throws \ReflectionException
   */
  protected function buildHookArgumentsInfo(string $hook_function): array {
    $function_definition = new \ReflectionFunction($hook_function);

    $map = [];
    foreach ($function_definition->getParameters() as $parameter) {
      $map[$parameter->getPosition()] = $parameter->getName();
    }

    return $map;
  }

  /**
   * Gets all discovered hook functions defined in .api.php files.
   *
   * @return array
   *   The list of discovered hooks.
   */
  private function getDiscoveredHookFunctions(): array {
    $exclude_functions = get_defined_functions()['user'];

    $this->loadExtensions();

    $hook_functions = get_defined_functions()['user'];

    return array_values(array_diff($hook_functions, $exclude_functions));
  }

  /**
   * Perform loading of all discovered extension files.
   *
   * Each file will have to be required in order to access
   * the hook functions defined in it.
   */
  private function loadExtensions(): void {
    foreach ($this->getExtensionFiles() as $api) {
      if (file_exists($api->uri)) {
        require_once $api->uri;
      }
    }
  }

}
