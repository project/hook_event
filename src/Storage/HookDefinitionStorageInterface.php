<?php

declare(strict_types=1);

namespace Drupal\hook_event\Storage;

/**
 * Provides the interface for the hooks definitions storages.
 */
interface HookDefinitionStorageInterface {

  /**
   * Gets all hooks definitions.
   *
   * @return array
   *   The list of all registered hooks definitions.
   */
  public function getHooksDefinitions(): array;

  /**
   * Gets a single hook definition.
   *
   * @param string $hook
   *   The name of the hook to search for.
   *
   * @return array
   *   The hook data.
   */
  public function getHookDefinition(string $hook): array;

  /**
   * Gets the arguments for the given hook.
   *
   * @param string $hook
   *   The name of the hook to search for.
   *
   * @return array
   *   The list of defined hook arguments info data.
   */
  public function getHookArguments(string $hook): array;

  /**
   * Check if the hook definition was defined.
   *
   * @param string $hook
   *   The name of the hook to search for.
   *
   * @return bool
   *   True if hook has registered definition, false otherwise.
   */
  public function hasHookDefinition(string $hook): bool;

}
