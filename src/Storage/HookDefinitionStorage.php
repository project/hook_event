<?php

declare(strict_types=1);

namespace Drupal\hook_event\Storage;

use Drupal\hook_event\Discovery\ApiDiscoveryInterface;

/**
 * Provides the storage wrapper for the discovered hook definitions.
 */
class HookDefinitionStorage implements HookDefinitionStorageInterface {

  /**
   * The API files discovery service.
   *
   * @var \Drupal\hook_event\Discovery\ApiDiscoveryInterface
   */
  protected ApiDiscoveryInterface $apiDiscovery;

  /**
   * Creates the storage instance.
   *
   * @param \Drupal\hook_event\Discovery\ApiDiscoveryInterface $api_discovery
   *   The API files discovery service.
   */
  public function __construct(ApiDiscoveryInterface $api_discovery,) {
    $this->apiDiscovery = $api_discovery;
  }

  /**
   * {@inheritdoc}
   */
  public function getHooksDefinitions(): array {
    return $this->apiDiscovery->getDefinitions();
  }

  /**
   * {@inheritdoc}
   */
  public function getHookDefinition(string $hook): array {
    return $this->getHooksDefinitions()[$hook] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getHookArguments(string $hook): array {
    return $this->getHookDefinition($hook)['arguments'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function hasHookDefinition(string $hook): bool {
    return (bool) $this->getHookDefinition($hook);
  }

}
