<?php

namespace Drupal\hook_event\EventSubscriber;

use Drupal\hook_event\Event\ApiDiscoveryEventInterface;
use Drupal\hook_event\Processor\HookDefinitionsProcessorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Defines the subscriber for the api discovery related events.
 */
class ApiDiscoveryEventSubscriber implements EventSubscriberInterface {

  /**
   * Constructs the api discovery subscriber.
   *
   * @param \Drupal\hook_event\Processor\HookDefinitionsProcessorInterface $hookDefinitionsProcessor
   *   The hook definition processor.
   */
  public function __construct(
    protected HookDefinitionsProcessorInterface $hookDefinitionsProcessor
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[ApiDiscoveryEventInterface::PRE_SAVE][] = ['onHookDefinitionsSave'];

    return $events;
  }

  /**
   * Allow to alter hook definitions before they are saved.
   *
   * @param \Drupal\hook_event\Event\ApiDiscoveryEventInterface $event
   *   The event object.
   */
  public function onHookDefinitionsSave(ApiDiscoveryEventInterface $event): void {
    $definitions = $event->getDiscoveredDefinitions();
    $this->hookDefinitionsProcessor->process($definitions);
    $event->setDiscoveredDefinitions($definitions);
  }

}
