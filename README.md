<!-- @codingStandardsIgnoreFile -->
# Hook to Event

This module is intended for the developers and allows them to
handle Drupal hooks in the event subscriber provided
by the Symfony framework.
All of the hooks in Drupal core that are invoked by the module handler
and by the theme handler (note: theme not yet implemented) can be now
registered in the subscriber by using the hook name.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/hook_event).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/hook_event).

## Table of contents

- Requirements
- Installation
- Configuration
- Usage
- Maintainers


## Requirements

The minimum PHP supported version is 8.0.
We will also require Drupal 9 or higher.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

**Using composer:**
```composer require drupal/hook_event```


## Configuration

Does not require any configuration.


## Usage

### Working with subscribers
Define the new event subscriber in your custom module as you usually
do in the `my_module.services.yml` file.
Register the event by using the hook name.
Example: In order to subscribe for the `hook_entity_update` you'll
have to define in your subscriber:
```php
public static function getSubscribedEvents(): array {
  return [
    'hook_entity_update' => ['myEntityUpdateHandlerMethod', 0]
  ];
}
```
In your method definition you can interact with the event like this:
```php
public function myEntityUpdateHandlerMethod(\Drupal\hook_event\Event\HookInvokeEventInterface $event): void {
  // Accessing by argument name (if hook was registered).
  $entity = $event->getArgument('entity');
  // Accessing by argument position (if hook wasn't registered).
  $entity = $event->getArgument(0);
  // The logic goes here...

  // Apply the changes (useful for non object variables).
  $event->setArgument('entity', $entity);

  // Set return value in case the hook require some return.
  $event->setReturnValue([...]);
}

```

### Register custom event class

The modules give you the ability to register your custom classes for
a certain hooks. In order to register it, you must:

1. Create the `{your_extension}.hook_events.yml`
2. Define the mapping where key is the hook name and the value is
   the event class name, ex.
```yaml
hook_preprocess_html: \Drupal\my_module\Event\HtmlPreprocessEvent
hook_entity_update: \Drupal\my_theme\Event\EntityUpdate
```
3. Clear the cache (via drush or UI)

**Important: The customized events must extend specific class or inherit the interface!**
For the:
1. Invoked hooks (just a regular hooks):
- `\Drupal\hook_event\Event\HookInvokeEvent` - if extending or,
- `\Drupal\hook_event\Event\HookInvokeEventInterface` if implementing interface.

2. Alter hooks:
- `\Drupal\hook_event\Event\HookAlterEvent` - if extending or,
- `\Drupal\hook_event\Event\HookAlterEventInterface` if implementing interface.

3. Preprocess hooks:
- `\Drupal\hook_event\Event\HookPreprocessEvent` - if extending or,
- `\Drupal\hook_event\Event\HookPreprocessEventInterface` if implementing interface.


## Maintainers

Current maintainers:
- Mariusz Andrzejewski - [sayco](https://www.drupal.org/u/sayco)
